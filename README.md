# DataHub Specifications

This repo contains the architecture specifications for the DataHub infrastructure.
It will reflect the current official development state as a reference for e.g. implementing parties, interested users, and other stakeholders.

## Concepts

- A module is an infrastructure component which has to fulfill certain tasks or features.
- An API specifies how determined kinds of information can be transferred between modules (communication).
- System architecture specifies the communication relationships between modules and which APIs are used in which relationship.
  See the [architecture docs](docs/architecture.md) for an overview.

## Licensing

This work is licensed under the [CC BY 4.0] license.

[cc by 4.0]: https://creativecommons.org/licenses/by/4.0/
